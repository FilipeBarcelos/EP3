class ComentariosController < ApplicationController
before_action :encontrar_produto
before_action :encontrar_comentario, only: [ :destroy, :edit, :update]

	def create
		@comentario = @produto.comentarios.create(params[:comentario].permit(:content))
		@comentario.usuario_id = current_usuario.id if current_usuario
		@comentario.save

		if @comentario.save
			redirect_to produto_path(@produto)
		else
			render 'new'
		end		
	end

	def destroy
		@comentario.destroy
		redirect_to produto_path(@produto)
	end	

	def edit
	end

	def update

		if @comentario.update(params[:comentario].permit(:content))
			redirect_to produto_path(@produto)
		else
			render 'edit'
		end		
	end	

	private

	def encontrar_produto
		@produto = Produto.find(params[:produto_id])
	end

	def encontrar_comentario
		@comentario = @produto.comentarios.find(params[:id])
	end	

end
