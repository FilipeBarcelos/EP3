class JogosController < ApplicationController
  before_action :authenticate_usuario!, only: [:contato]

  def home
  end

  def sobre
  end

  def perguntas
  end	

end
