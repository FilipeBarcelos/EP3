json.extract! produto, :id, :titulo, :descricao, :imagem_url, :valor, :categoria, :subcategoria, :created_at, :updated_at
json.url produto_url(produto, format: :json)