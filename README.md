
-> Tema e ideia:
Escolhemos criar um site para a venda de jogos digitais, devido a gra
nde procura e estar inserida no meio tecnológico.
Caso alguém tenha o interesse de adquirir jogos com preços acessíveis a todos
além de também poder ver promoções de outros jogos esse site será perfeito para
isso. Nossa ideia é de que um usuário não cadastrado pode somente ver as
promoções dos principais produtos, porém o usuário cadastrado, além de poder
ver as promoções, também pode adquirir algum produto o qual teve interesse e
comentar e dar sua opinião sobre o que achou do jogo. Alem dos usuários normais
temos usuários administradores,os quais são encarregados de cadastrar as
promoções e produtos que estão no site,assim como ter o controle de quantos
usuários normais estão utilizando o site, e tendo total acesso e controle sobre os
comentários. Gostariamos de fazer uma separação de plataformas e melhorado o
design da loja virtual,mas devido aos contratempos enfrentados e a falta de tempo
para adquirirmos conhecimentos sobre a linguagem não conseguimos executar
tudo o que queríamos.

-> Funcionalidades e como navegar pelo site: O funcionamento do nosso site
funciona de forma parecida com a de uma loja virtual, ou seja, um usuário
cadastrado tem mais privilégios que um usuário não cadastrado.

- Como ver um produto:
Qualquer usuário pode ver qualquer produto postado no site e elas apa
recem listadas nos produtos. Para ter mais informações sobre o produto para v
er se ele lhe interessa ou se faz parte do estilo que gosta, poderá ler a des
crição e os comentários escritos por aqueles usuários que já utilizaram deter
minado jogo.

- Como adquirir um produto:
Para adquirir um produto, o usuário deve clicar no link "Produtos” e
deverá clicar no link “adicionar ao carrinho” (o usuário deverá estar logado
para efetuar tal ação). Esse link redireciona o usuário para outra página
que contem os jogos que ele pretende adquirir ,o valor total e a quantidade
dos jogos.

Depois de ter adicionado ao carrinho os Jogos que o usuário cadastrado deseja
r basta finalizar a compra.
Caso o Usuário tenha gostado ou mesmo não tenha,ele poderá dividir suas ex
periências com outros usuários através dos comentários

-Como criar um comentário:
Para comentar determinado produto o usuário deve estar logado e entra
r na página produtos.
No final da tela se encontra um campo de texto editável e é onde o usuário de
verá digitar seu texto. Após digitá-lo, o comentário só irá aparecer para os
outros usuários ao apertar no botão "Comentar".

- Como se cadastrar, entrar e sair:
Para se cadastrar, o usuário deve clicar no botão "Cadastrar", locali
zado no canto superior direito da tela e preencher os campos de email e senha
, que devem conter no mínimo 6 caracteres.
Após efetuado o cadastro, o usuário automaticamente já estará logado,
porém, caso ele saia e não esteja mais logado, ele deve clicar no botão "Entrar ”,
 também localiza-do no canto superior direito da tela, e preencher os campos
com seu email e senha cadastrados. Caso ele queira encerrar sua sessão o
usuário deve clicar no botão "Sair".

 Editando sua conta:
Para saber se você está logado no site, basta dar uma olhada para a barra
superior do site, pois sempre que um usuário está logado as opções de cadastrar e
Entrar desaparecem. Sabendo disso, caso um usuário queira fazer modificações
em sua conta ele pode, basta que ele clique no link “Minha Conta”. Ao clicar o
usuário é direcionado para outra página, onde ele pode mudar sua senha, mudar
seu email ou deletar toda sua conta. Ao deletar uma conta, o usuário não consegue
mais entrar no sistema com o email que estava cadastrado, sendo que ele
deve se cadastrar novamente para poder ter todos os privilégios que ele tinha.
Além disso, ao deletar uma conta, todos os posts e comentários associados a essa
conta também são deletados.

 Usuário Administrador:
O usuário administrador é um usuário especial para o sistema, pois ele tem
permissões para fazer o que quiser em todo o site. Esse atributo de administrador
é tão forte que um usuário só pode ganhá-lo por meio do console do Rails. O 
administrador, além de ter todos os privilégios de um usuário cadastrado comum,
também pode editar e deletar tudo de qualquer tipo de comentário que achar não
adequado e tem o poder de cadastrar todos os produtos , ou seja, o administrador
pode moderar tudo o que se passa no site, desde comentários até mesmo os
produtos.

 Sobre:
Outro botão localizado na parte superior do site, que aparece para qualquer
usuário é o “Sobre”. Ao clicar nesse botão o usuário é redirecionado para uma
página que conta um pouco mais sobre o site e seus desenvolvedores, caso o
usuário queira saber um pouco mais sobre nós.

 Gems:
Utilizamos as gems default do Rails e adquirimos somente o Devise, para criar
usuários e gerar permissões.